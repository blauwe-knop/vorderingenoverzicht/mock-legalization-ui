# mock-legalization-ui

Procescomponent of participating organizations in the 'Vorderingenoverzicht' system.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)

1. Download the required Go dependencies:

```sh
go mod download
```

1. Now start the mock legalization ui:

```sh
go run cmd/mock-legalization-ui/main.go
```

By default, the mock legalization ui will run on port `80`.

## Deployment
