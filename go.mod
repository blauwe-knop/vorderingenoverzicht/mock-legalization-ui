module gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-ui

go 1.20

require (
	github.com/go-chi/chi/v5 v5.0.10
	github.com/jessevdk/go-flags v1.5.0
	gitlab.com/blauwe-knop/common/health-checker v0.0.3
	go.uber.org/zap v1.25.0
)

require (
	gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-process v0.12.1
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
)
