FROM golang:1-alpine AS build

RUN apk add --update --no-cache git

ADD . /go/src/mock-legalization-ui/
WORKDIR /go/src/mock-legalization-ui
RUN go mod download
RUN go build -o dist/bin/mock-legalization-ui ./cmd/mock-legalization-ui

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/mock-legalization-ui/dist/bin/mock-legalization-ui /usr/local/bin/mock-legalization-ui
COPY --from=build /go/src/mock-legalization-ui/html/templates/login.html /html/templates/login.html
COPY --from=build /go/src/mock-legalization-ui/html/templates/legalize.html /html/templates/legalize.html
COPY --from=build /go/src/mock-legalization-ui/html/templates/legalized.html /html/templates/legalized.html

# Add non-priveleged user. Disabled for openshift
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/mock-legalization-ui"]
