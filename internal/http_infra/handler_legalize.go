// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"html/template"
	"log"
	"net/http"
	"path"
	"path/filepath"
)

type performLegalizePage struct {
	LogoURL      string
	LegalizedURL template.URL
}

func handlerLegalizePage(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	templateDirectory, _ := ctx.Value(htmlTemplateDirectoryKey).(string)
	organizationLogoURL, _ := ctx.Value(organizationLogoUrlKey).(string)
	bsn := r.URL.Query().Get("bsn")

	if bsn == "" {
		log.Printf("bsn required")
		http.Error(w, "bsn required", http.StatusBadRequest)
		return
	}

	templatePath := filepath.Join(templateDirectory, "legalize.html")
	filePath := path.Join(templatePath)

	parsedTemplate, err := template.ParseFiles(filePath)
	if err != nil {
		log.Printf("error parsing template: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	err = parsedTemplate.Execute(w, &performLegalizePage{
		LogoURL:      organizationLogoURL,
		LegalizedURL: template.URL("/legalized?bsn=" + bsn),
	})

	if err != nil {
		log.Printf("error serving template: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}
}
