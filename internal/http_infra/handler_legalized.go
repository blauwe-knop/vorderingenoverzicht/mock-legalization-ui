// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"html/template"
	"log"
	"net/http"
	"path"
	"path/filepath"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-process/pkg/model"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-ui/internal/repositories"
)

type performLegalizedPage struct {
	LogoURL   string
	VORijkURL template.URL
}

func handlerLegalizedPage(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	legalizationProcessRepository, _ := ctx.Value(legalizationProcessRepositoryKey).(repositories.LegalizationProcessRepository)
	templateDirectory, _ := ctx.Value(htmlTemplateDirectoryKey).(string)
	organizationLogoURL, _ := ctx.Value(organizationLogoUrlKey).(string)
	bsn := r.URL.Query().Get("bsn")

	cookie, err := r.Cookie("legalizatorToken")
	if err != nil {
		log.Printf("legalizatorToken cookie is missing or expired - the page could not be loaded: %v", err)
		http.Error(w, "legalizatorToken cookie is missing or expired - the page could not be loaded", http.StatusInternalServerError)
		return
	}

	err = legalizationProcessRepository.CompleteLegalization(cookie.Value, model.UserIdentity{Bsn: bsn})
	if err != nil {
		log.Printf("error completing legalization: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	templatePath := filepath.Join(templateDirectory, "legalized.html")
	filePath := path.Join(templatePath)

	parsedTemplate, err := template.ParseFiles(filePath)
	if err != nil {
		log.Printf("error parsing template: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	redirectUrl := "vorijk://vorderingenoverzicht.app/wizard/legalizator/complete"
	err = parsedTemplate.Execute(w, &performLegalizedPage{
		LogoURL:   organizationLogoURL,
		VORijkURL: template.URL(redirectUrl),
	})

	if err != nil {
		log.Printf("error serving template: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}
}
