// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-ui/internal/repositories"
)

type key int

const (
	htmlTemplateDirectoryKey         key = iota
	legalizationProcessRepositoryKey key = iota
	organizationLogoUrlKey           key = iota
	loginURLKey                      key = iota
	callbackURLKey                   key = iota
)

type RouterConfig struct {
	LogoURL                       string
	LegalizationProcessRepository repositories.LegalizationProcessRepository
	HTMLTemplateDirectory         string
	LoginURL                      string
	CallbackURL                   string
}

func NewRouter(config RouterConfig) *chi.Mux {
	r := chi.NewRouter()

	r.Route("/login", func(r chi.Router) {
		r.Use(middleware.Logger)
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), htmlTemplateDirectoryKey, config.HTMLTemplateDirectory)
			ctx = context.WithValue(ctx, organizationLogoUrlKey, config.LogoURL)
			ctx = context.WithValue(ctx, loginURLKey, config.LoginURL)
			ctx = context.WithValue(ctx, callbackURLKey, config.CallbackURL)
			handlerLoginPage(w, r.WithContext(ctx))
		})
	})

	r.Route("/legalize", func(r chi.Router) {
		r.Use(middleware.Logger)
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), htmlTemplateDirectoryKey, config.HTMLTemplateDirectory)
			ctx = context.WithValue(ctx, organizationLogoUrlKey, config.LogoURL)
			ctx = context.WithValue(ctx, loginURLKey, config.LoginURL)
			handlerLegalizePage(w, r.WithContext(ctx))
		})
	})

	r.Route("/legalized", func(r chi.Router) {
		r.Use(middleware.Logger)
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), htmlTemplateDirectoryKey, config.HTMLTemplateDirectory)
			ctx = context.WithValue(ctx, legalizationProcessRepositoryKey, config.LegalizationProcessRepository)
			ctx = context.WithValue(ctx, organizationLogoUrlKey, config.LogoURL)
			ctx = context.WithValue(ctx, loginURLKey, config.LoginURL)
			handlerLegalizedPage(w, r.WithContext(ctx))
		})
	})

	healthCheckHandler := healthcheck.NewHandler("mock-legalization-ui", []healthcheck.Checker{})
	r.Route("/health", func(r chi.Router) {
		r.Get("/", healthCheckHandler.HandleHealth)
		r.Get("/check", healthCheckHandler.HandlerHealthCheck)
	})

	return r
}
