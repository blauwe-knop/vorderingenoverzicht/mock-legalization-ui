// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"html/template"
	"log"
	"net/http"
	"path"
	"path/filepath"
	"time"
)

type performLoginPage struct {
	LogoURL  string
	LoginURL string
}

func handlerLoginPage(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	templateDirectory, _ := ctx.Value(htmlTemplateDirectoryKey).(string)
	organizationLogoURL, _ := ctx.Value(organizationLogoUrlKey).(string)
	loginURL, _ := ctx.Value(loginURLKey).(string)
	callbackURL, _ := ctx.Value(callbackURLKey).(string)

	legalizatorToken := r.URL.Query().Get("token")
	if legalizatorToken == "" {
		log.Printf("no legalizatorToken provided")
		http.Error(w, "no legalizatorToken provided", http.StatusBadRequest)
		return
	}

	templatePath := filepath.Join(templateDirectory, "login.html")
	filePath := path.Join(templatePath)

	parsedTemplate, err := template.ParseFiles(filePath)
	if err != nil {
		log.Printf("error parsing template: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	expiration := time.Now().Add(10 * time.Minute)
	cookie := http.Cookie{Name: "legalizatorToken", Value: legalizatorToken, Expires: expiration, SameSite: http.SameSiteStrictMode}
	http.SetCookie(w, &cookie)

	err = parsedTemplate.Execute(w, &performLoginPage{
		LogoURL:  organizationLogoURL,
		LoginURL: loginURL + "?callbackURL=" + callbackURL + "/legalize",
	})

	if err != nil {
		log.Printf("error serving template: %v", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}
}
