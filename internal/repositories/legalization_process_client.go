// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	processmodel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-process/pkg/model"
)

type LegalizationProcessClient struct {
	baseURL string
}

var ErrOrganizationNotFound = errors.New("organization does not exist")

func NewLegalizationProcessClient(baseURL string) *LegalizationProcessClient {
	return &LegalizationProcessClient{
		baseURL: baseURL,
	}
}

func (s *LegalizationProcessClient) CompleteLegalization(token string, completedLegalization processmodel.UserIdentity) error {
	url := fmt.Sprintf("%s/complete_legalization", s.baseURL)

	requestBodyAsJson, err := json.Marshal(completedLegalization)
	if err != nil {
		return fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return fmt.Errorf("failed to post completedLegalization request: %v", err)
	}

	request.Header.Set("Authorization", token)
	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return fmt.Errorf("failed to post completedLegalization: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected status while retrieving completedLegalization: %d", resp.StatusCode)
	}

	return nil
}
