// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	processmodel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-process/pkg/model"
)

type LegalizationProcessRepository interface {
	CompleteLegalization(string, processmodel.UserIdentity) error
}
