// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-ui/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-legalization-ui/internal/repositories"
)

type options struct {
	ListenAddress              string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the mock-legalization-ui api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	LegalizationProcessAddress string `long:"legalization-process-address" env:"LEGALIZATION_PROCESS_ADDRESS" default:"http://localhost:80" description:"Legalization process address."`
	OrganizationLogoURL        string `long:"organization-logo-url" env:"ORGANIZATION_LOGO_URL" description:"Organization logo URL"`
	LoginURL                   string `long:"login-url" env:"LOGIN_URL" description:"Login url"`
	CallbackURL                string `long:"callback-url" env:"CALLBACK_URL" description:"Callback url"`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	legalizationProcess := repositories.NewLegalizationProcessClient(cliOptions.LegalizationProcessAddress)

	routerConfig := http_infra.RouterConfig{
		LegalizationProcessRepository: legalizationProcess,
		LogoURL:                       cliOptions.OrganizationLogoURL,
		LoginURL:                      cliOptions.LoginURL,
		CallbackURL:                   cliOptions.CallbackURL,
		HTMLTemplateDirectory:         "html/templates/",
	}

	router := http_infra.NewRouter(routerConfig)

	logger.Info(fmt.Sprintf("start listening on %s", cliOptions.ListenAddress))

	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
